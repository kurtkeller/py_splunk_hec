py_splunk_hec
=============

Python module to easily send log events from python scripts to
splunk> via the splunk> HTTP Event Collector (HEC).
