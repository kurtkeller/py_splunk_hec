#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set syntax=python et sm ai ci ru sw=4 tw=76 fileencodings=utf-8:

from perpetual_timer import PerpetualTimer
import requests
import time
import uuid
import json

# ==========================================================================
class logger():
    """
    class to send logs to a splunk> HTTP Event Collector

    Basic use
    ---------

    1) initialize the logger
       > mylogger = logger(
       >                token = "DF506393-463F-4B2C-B01D-48B5ECA74FE4",
       >                index = "hec_test",
       >                host = "myhostname",
       >                source = "mysource",
       >                sourcetype = "mysourcetype",
       >                urlbase = "https://splunk-hec.mydomain.com:58584",
       >                )

    2) write a log line
       - we log in JSON format, thus all logs must be dictionaries
       - the log line is not actually written right away, but added
         to a queue (inqueue); the inqueue is processed
       - ATTENTION! we add a field "retry" to the dictionary with the
                    actual log entry; it will become part of the log
                    entry an if the value is > 0, it indicates that
                    the log entry was sent as a retry, because we did
                    not get acknowledgements from the HTTP Event
                    Collector for the previously sent copies; so
                    if retry > 0 this _could_ be a duplicate log
                    entry
              
       > mylogger.log({"user": "me",
       >               "action": "login",
       >               "result": "success",
       >              })
       - the initialized defaults for index, source, sourcetype and host
         can be overridden when queueing a log message:
       > mylogger.log({"user": "me",
       >               "action": "login",
       >               "result": "success",
       >              },
       >              index = "hec_test2",
       >              source = "othersource",
       >              sourcetype = "othersourcetype",
       >              host = "otherhost",
       >             )

    3) every XX seconds (default: 5) a function wakes up to process the
       current queues and
       a) send the entries in the inqueue to the HTTP Event Collector
       b) fetch acknowledgements from the HTTP Event Collector and
          remove acknowledged events from the buffer
       c) check the remaining buffer to see whether any entry has not
          been acknowledged for XX seconds (default: 300) and if so
          resend it to the HTTP Event Collector
          This might cause duplicates! 
          The value of the "retry" field will be increased by one
          for each resend so possible duplicates are marked in splunk>.

    4) when you are done, then shutdown the logger; this causes the
       queues to be processed until all queues have been emptied; if you
       just end the program without calling shutdown, there may be unsent
       log entries which will get lost
       > mylogger.shutdown()

       ATTENTION! It can take a few minutes until all the messages are
                  acknowledged by the HTTP Event Collector, be patient if
                  you want to be sure.
       You might want to do a flush afer a shutdown to make sure you
       wait with exiting etc. until everything has been acknowledged; flush
       will block till then, shutdown does not (however, the script should
       not exit before everything is flushed, even with shutdown).
       > mylogger.flush()

    -  you can manuall flush the queue:
       > logger.flush()
       this will wait hammer the HTTP Event Collector until all queues could
       be send and acknowledged

    -  you can manually stop the queue processing timer
       > logger.stop()
       ATTENTION! This will _not_ flush the queue!

    -  you can manually start the queue processing timer again
       > logger.start()

    -  you can manually process the queue once
       > logger.process()

    -  you can get the number of items currently in the queues with
       > logger.get_queue_sizes()

    -  you can check the size of the queue, which is the sum of the
       not yet processed log entries plus the not yet acknowledged
       log entries
       > len(logger)

    - you can print the contents of the queue
      > print logger

    - you can get the internal details of the queue, including retry
      counts, last message/error received, etc
      > logger.get_queue_details()

    - you can get a health status of the HTTP Event Collctor with
      > logger.get_collector_health()

    """
    # ----------------------------------------------------------------------
    def __init__(self,
        token,
        index,
        host,
        source,
        sourcetype,
        urlbase,
        channel = None,
        sslverify = False,
        checkack = False,
        timer = 5,
        retry_timer = 300,
        maxbatchsize = 10000,
    ):

        """
        token:          the UUID token for authentication to HEC
        channel:        the channel to use
                        if none is passed, one will be calculated
                        from the token with a random node part
        index:          the default index to send to
        host:           the default host to specify
        source:         the default source to specif
        sourcetype:     the default sourcetype to specify
        urlbase:        the base url to the splunk> HTTP Event Collector
                        eg: https://splunk-hec.mydom.com:58584
        sslverify       do verification on SSL connections (True) or
                        not (False)
                        default: False
        checkack        do verify acknowledgements (True) or
                        not (False)
                        default: False
        timer           run the queue processor every XX seconds
                        default: 5
        retry_timer     retry sending log entries every XX seconds
                        if no ack has been received for the previous
                        copies
                        default: 300
        maxbatchsize    the maximum number of log entries to push
                        from the workqueue and check for acks per run
                        setting this to 0 means unlimited
                        default: 10000
        """

        # the passed values
        self.token              = token
        self.channel            = channel
        self.index              = index
        self.host               = host
        self.source             = source
        self.sourcetype         = sourcetype
        self.urlbase            = urlbase
        self.timer              = timer
        self.retry_timer        = retry_timer
        self.maxbatchsize       = maxbatchsize

        # values computed from passed values
        if self.channel == None:
            self.channel = self.token.rsplit("-",1)[0] + "-" + str(
                                             uuid.uuid4()).split("-")[-1]
        self.logurl     = self.urlbase + "/services/collector/event"
        self.ackurl     = self.urlbase + "/services/collector/ack"
        self.healthurl  = self.urlbase + "/services/collector/health"
        self.headers    = {'Authorization': "Splunk " + self.token,
                           'X-Splunk-Request-Channel': self.channel,
                          }
        # session object with defaults for the connection
        self.session    = requests.Session()
        self.session.headers.update(self.headers)

        self.sslverify = sslverify
        if not sslverify:
            self.session.verify = False
            requests.packages.urllib3.disable_warnings(category = 
                requests.packages.urllib3.exceptions.InsecureRequestWarning)

        self.checkack = checkack

        # the various queues/buffers/flags
        self.inqueue = []
        self.workqueue = []
        self.ackqueue = []
        self.lock_inqueue = False
        self.lock_workqueue = False
        self.lock_ackqueue = False
        self.in_shutdown = False

        # setup the timer
        self.auto_process = PerpetualTimer(self.timer, self.process)
        # star the timer
        self.auto_process.start()
        
    # ----------------------------------------------------------------------
    def __len__(self):
        return (len(self.ackqueue) + len(self.workqueue) + len(self.inqueue))

    # ----------------------------------------------------------------------
    def len(self):
        return(self.__len__())

    # ----------------------------------------------------------------------
    def get_collector_health(self):

        try:
            health = self.session.get(self.healthurl,
                                     timeout=(3.5,1.5),
                                    )
            return ({"status_code": health.status_code,
                     "text":        health.json()["text"],
                   })
        except Exception as ex:
            return ({"status_code": 999,
                     "text": "%s" % ex,
                   })

    # ----------------------------------------------------------------------
    def get_queue_details(self):
        result = {}
        result["ackqueue"] = self.ackqueue
        result["workqueue"] = self.workqueue
        result["inqueue"] = self.inqueue

        return (result)

    # ----------------------------------------------------------------------
    def __print__(self):
        result = {}
        result["ackqueue"] = []
        for item in self.ackqueue:
            result["ackqueue"].append(item["logmsg"])
        result["workqueue"] = []
        for item in self.workqueue:
            result["workqueue"].append(item["logmsg"])
        result["inqueue"] = []
        for item in self.inqueue:
            result["inqueue"].append(item["logmsg"])

        return (json.dumps(result, indent=2))

    # ----------------------------------------------------------------------
    def __repr__(self):
        return(self.__print__())

    # ----------------------------------------------------------------------
    def get_queue_sizes(self):
        return ({"inqueue": len(self.inqueue),
                 "workqueue": len(self.workqueue),
                 "ackqueue": len(self.ackqueue),
               })

    # ----------------------------------------------------------------------
    def flush(self):
        while self.__len__():
            self.process()

    # ----------------------------------------------------------------------
    def stop(self):
        self.auto_process.cancel()
        #self.flush() let the user decide not to flush or flush manually

    # ----------------------------------------------------------------------
    def start(self):
        self.in_shutdown = False
        self.auto_process.start()

    # ----------------------------------------------------------------------
    def shutdown(self):
        self.in_shutdown = True

    # ----------------------------------------------------------------------
    def log(self,
            logmsg,
            index       = None,
            host        = None,
            source      = None,
            sourcetype  = None
           ):

        """
        log(logmsg, index, host, source, sourcetype)

        logmsg:         mandatory
                        must be a dictionyry

        index:          optional
                        must be a string
                        if specified, overrides the initilized default

        host:           optional
                        must be a string
                        if specified, overrides the initilized default

        source:         optional
                        must be a string
                        if specified, overrides the initilized default

        sourcetype:     optional
                        must be a string
                        if specified, overrides the initilized default
        """

        if self.in_shutdown:
            raise StandardError("logger is in shutdown")

        now = time.time()

        if index == None:
          index = self.index
        if host == None:
          host = self.host
        if source == None:
          source = self.source
        if sourcetype == None:
          sourcetype = self.sourcetype

        # we add a retry value to the event in case we need to retry
        # sending it, in which case it could lead to double entries
        if not "retry" in logmsg:
             logmsg["retry"] = -1
             retry_added = True
        else:
             retry_added = False

        di_logentry = {
                 "uuid":               str(uuid.uuid1()),
                  "retry_added":        retry_added,
                  "retrycount":         -1,
                  "time_received":      now,
                  "first_attempt":      None,
                  "last_attempt":       None,
                  "ackId":              None,
                  "previous_ackIds":    [],
                  "last_message":       "",
                  "logmsg":             {
                        "host":                 host,
                        "time":                 int(now),
                        "source":               source,
                        "sourcetype":           sourcetype,
                        "index":                index,
                        "event":                logmsg,
                                        },
                      }


        # thread safety; wait for the lock to be released
        while self.lock_inqueue:
            pass
        self.lock_inqueue = True
        self.inqueue.append(di_logentry)
        self.lock_inqueue = False


    # ----------------------------------------------------------------------
    def process(self):

        self._process_inqueue()
        self._process_workqueue()
        self._process_ackqueue()

        if (self.in_shutdown) and (len(self) == 0):
            self.auto_process.cancel()

    # ----------------------------------------------------------------------
    def _process_inqueue(self):

        # thread safety; wait for the lock to be released
        while self.lock_workqueue:
            pass
        self.lock_workqueue = True

        # thread safety; wait for the lock to be released
        while self.lock_inqueue:
            pass
        self.lock_inqueue = True

        # move new things to a different queue so the producers are
        # free to write new log entries again as quickly as
        # possible, even while we are working on actually sending
        # the queued messages
        self.workqueue.extend(self.inqueue)
        self.inqueue = []
        self.lock_inqueue = False
        self.lock_workqueue = False

    # ----------------------------------------------------------------------
    def _process_workqueue(self):

        # thread safety; wait for the lock to be released
        while self.lock_ackqueue:
            pass
        self.lock_ackqueue = True

        # thread safety; wait for the lock to be released
        while self.lock_workqueue:
            pass
        self.lock_workqueue = True

        batchcounter = 0
        for item in self.workqueue[:]:
            batchcounter += 1
            if self._send_item(item):
                self.workqueue.remove(item)
                # if we don't want to verify acks, then
                # we just don't add the entries to the ackqueue
                if self.checkack:
                    if item["ackId"] != "-":
                        self.ackqueue.append(item)
            if (0 < self.maxbatchsize <= batchcounter):
                break

        self.lock_ackqueue = False
        self.lock_workqueue = False

    # ----------------------------------------------------------------------
    def _process_ackqueue(self):

        # thread safety; wait for the lock to be released
        while self.lock_ackqueue:
            pass
        self.lock_ackqueue = True

        now = time.time()
        # check acks and remove acknowledged entries
        batchcounter = 0
        for item in self.ackqueue[:]:
            batchcounter += 1
            if self._check_ack(item):
                self.ackqueue.remove(item)
            if (0 < self.maxbatchsize <= batchcounter):
                break

        # check for items long time not acknowledged and resend them
        batchcounter = 0
        for item in self.ackqueue[:]:
            if item["last_attempt"] + self.retry_timer < now:
                batchcounter += 1
                self._send_item(item)
                if (0 < self.maxbatchsize <= batchcounter):
                    break

        self.lock_ackqueue = False

    # ----------------------------------------------------------------------
    def _send_item(self, item):

        now = time.time()
        if item["first_attempt"] == None:
            item["first_attempt"] = now
        item["last_attempt"] = now
        item["retrycount"] += 1
        if item["retry_added"]:
            item["logmsg"]["event"]["retry"] += 1
        try:
            logentry = self.session.post(self.logurl,
                                     data=json.dumps(
                                        item["logmsg"],indent=2),
                                     timeout=(3.5,1.5),
                                    )
            item["last_message"] = logentry.text
            #todo: KK: should possibly return some error indication if
            #          status_code was not 200
            if logentry.status_code == 200:
                if "ackId" in logentry.json():
                    ackId = logentry.json()['ackId']
                else:
                    ackId = "-"
            else:
                ackId = None
        except Exception as ex:
            ackId = None
            item["last_message"] = "%s" % ex

        if ackId == None:
            # not sent successfully, reduce the retry parameter again
            if item["retry_added"]:
                item["logmsg"]["event"]["retry"] -= 1
        else:
          if item["ackId"] != None:
              item["previous_ackIds"]. append(item["ackId"])
          item["ackId"] = ackId

        if ackId == None:
          return False  # unsuccessful
        else:
          return True   # successfull

        
    # ----------------------------------------------------------------------
    def _check_ack(self, item):

        ackId = item["ackId"]
        if ackId == None:
            return False
        ackId = int(ackId)

        try:
            ackresult = self.session.post(
                            self.ackurl,
                            data=json.dumps({"acks": [ackId]}),
                            timeout=(3.5,1.5),
                        )
            if ackresult.status_code == 200:
               return ackresult.json()["acks"]["%s" % ackId]
            else:
                return False
        except:
            return False


# ==========================================================================
if __name__ == "__main__":

    import uuid
    import socket
    import sys
    l=logger(
        token = "DF506393-463F-4B2C-B01D-48B5ECA74FE4",
        #channel = "DF506393-463F-4B2C-B01D-" + str(
        #                                     uuid.uuid4()).split("-")[-1],
        index = "hec_test",
        host = socket.gethostname().split(".")[0],
        source = sys.argv[0],
        sourcetype = "my_sourcetype",
        urlbase = "https://hec-t.splunk.essd.ch:58495",
        #sslverify = True,
        #checkack = True,
        #maxbatchsize = 10000,
    )

    print l.get_collector_health()["text"]

    print "size of queues: %d" % len(l)
    print

    for c in range(7):
        for i in range(7):
            l.log({"user": "user%03d" % (c*i),
                   "loop": c,
                   "round": i,
                   "time": time.strftime("%Y-%m-%dT%H:%M:%S%z"),
                 })
            time.sleep(0.2)
        print time.ctime()
        print json.dumps(l.get_queue_sizes(),indent=2)
        print
        time.sleep(1.5)

    l.shutdown()
    #print l.get_queue_details()
    while len(l):
        print "shutdown still in progress at %s" % time.ctime()
        print json.dumps(l.get_queue_sizes(), indent=2)
        print l.get_collector_health()["text"]
        time.sleep(2)
