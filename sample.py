#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set syntax=python et sm ai ci sw=4 tw=76 fileencodings=utf-8:

import splunk_hec
import uuid
import socket
import os



logger = splunk_hec.logger(
        token           = "DF506393-463F-4B2C-B01D-48B5ECA74FE4",
        #channel         = "DF506393-463F-4B2C-B01D-" + str(
        #                                        uuid.uuid4()).split("-")[-1],
        index           = "hec_test",
        host            = socket.gethostname().split(".")[0],
        source          = "test_service",
        sourcetype      = "general",
        urlbase         = "https://hec-t.splunk.example.com:8088",
        #sslverify       = True,
        #checkack        = False,
)

print logger.get_collector_health()["text"]

while True:
  reply = raw_input("message to log ('quit' to exit): ")
  if reply == "quit":
    break
  logger.log( {"message":   reply,
               "user":      os.getlogin(),
               "uid":       os.getuid(),
               "cwd":       os.getcwd(),
               "gid":       os.getgid(),
               "groups":    os.getgroups(),
               "pid":       os.getpid(),
            })

logger.shutdown()
print logger.get_collector_health()["text"]
print logger.get_queue_details()
logger.flush()
    
