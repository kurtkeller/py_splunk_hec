USE
===

A very simple sample script is given in the file [sample.py](sample.py).
This script only uses the minimum of initialization parameters. The
remaining parameters are described later in this file.

The minimum steps are:
1. load the module
```python
import splunk_hec
```

2. initialize a logger object, passing parameters to it as needed by
   your environment
```python
logger = splunk_hec.logger(
        token           = "DF506393-463F-4B2C-B01D-48B5ECA74FE4",
        index           = "hec_test",
        host            = "myhost.example.com",
        source          = "test_service",
        sourcetype      = "general",
        urlbase         = "https://hec-t.splunk.example.com:8088",
)
```
3. call the `log` method of the initialized logger object, passing
   the log entries to it in JSON format (python dictionary)
```python
logger.log(
      { "dst_user":      "uex0000",
        "src_ip":        "172.19.144.145",
        "destination":   "test_app_01",
        "action":        "login",
        "result":        "fail",
        "reason":        "wrong password",
        "failcount":     1,
        "msg":           "user uex000 attempted login with wrong password",
      }
)
```

optional but recommended:
*. when done, call the `shutdown` method of the logger object to
   prevent new log messages to be passed to it
```python
logger.shutdown()
```

*. call the `flush` method of the logger object to flush all the
   queues
```python
logger.flush()
```

Before your script actually exits, it may take some time. It will not
terminate until all the queues are flushed (even without calling the
`flush` method).


Initialization Parameters
=========================

The following parameters are available when initializing the logger
object:


| Parameter       | type    | requirement | default | description |
| --------------- | ------- | ----------- | --------| ----------- |
| `token`         | string  | mandatory   |         | the token to authenticate to HEC |
| `index`         | string  | mandatory   |         | the index to write the data to |
| `host`          | string  | mandatory   |         | the value for the `host` field in splunk> |
| `source`        | string  | mandatory   |         | the value for the `source` field in splunk> |
| `sourcetype`    | string  | mandatory   |         | the value for the `sourcetype ` field in splunk> |
| `urlbase`       | string  | mandatory   |         | the URL at which the HEC is listening: `protocol://host:port` but no `endpoint` |
| `channel`       | string  | optional    | same as `token` but with the last part (node) calculated randomly | the channel-Id to use for indexer acknowledgement |
| `sslverify`     | boolean | optional    | False   | True to verify SSL certificates etc., False not to verify them |
| `checkack`      | boolean | optional    | False   | True to actually check for indexer acknowledgement, False to ignore them |
| `timer`         | integer | optional    |       5 | every `timer` seconds a thread wakes up to process the queues |
| `retry_timer`   | integer | optional    |     300 | if `checkack` is set to True and an acknowlegement has not been received for `retry_timer` seconds, the event is resent |
| `maxbatchsize`  | integer | optional    |   10000 | during one execution process at most `maxbatchsize` events or acknowledgements per queue |

Parameters with the `log` Method
================================

When calling the `log` method of the logger object, the only mandatory
parameter is the event to log (in JSON format, which simply is a python
dictionary). However, some initialized parameters can be overridden on
a per-call basis:

| Parameter       | type       | requirement | default | description |
| --------------- | ---------- | ----------- | --------| ----------- |
| `logmsg`        | dictionary | mandatory   |         | the actual log event |
| `index`         | string     | mandatory   |         | the index to write the data to |
| `host`          | string     | mandatory   |         | the value for the `host` field in splunk> |
| `source`        | string     | mandatory   |         | the value for the `source` field in splunk> |
| `sourcetype`    | string     | mandatory   |         | the value for the `sourcetype ` field in splunk> |

The time for the event (`_time` field in splunk>), will be the current
system time.


Exposed Methods
===============

The following methods of the logger object are available:

| call | return value | description | 
| ---- | ------------ | ----------- |
| logger.log( {"key": "value" ... } ) | nothing returned | send the passed dictionary as log event to splunk> |
| logger.shutdown()             | nothing returned      | prepare the logger for shutdown; it will not accept any more logs for processing, but continue to process the queues regularly in the background
| logger.flush()                | nothing returned      | flush all the queues; this can be a lengthy process and will block until all queues are empty
| len(logger)                   | integer               | return the number of entries in all queues
| print logger                  | json formatted string | print the contents of the queues
| logger.get_collector_health() | dictionary            | query the HEC for its health status and return the result
| logger.get_queue_details()    | dictionary            | get the contents of the queue with all details
| logger.get_queue_sizes()      | dictionary            | get the size of all the queues
| logger.stop()                 | no return value       | stop processing the queues in the background; does __not__ flush the queues
| logger.start()                | no return value       | restart processing the queues
| logger.process()              | no return value       | force processing the queues one time


Theory of Operation
===================

When initializing a logger object, it starts a timer which will process
the queues every `timer` seconds.

When the `log` method is called, the passed log message is added to the
inqueue. It is not sent right away, so the caller is not blocked for too
long and can continue its work quickly.

Every time the timer wakes up, it first copies all the collected messages
from the inqueue to the workqueue. This is because we want to block the
inqueue for as little time as possible so the main part of the script is
not blocked too much by the timer.

Then the process tries to send each message in the workqueue to the HEC,
at most `maxbatchsize` messages per run. If a message could not be
successfully sent to HEC, it remains in the workqueue for the next run.
If it could be sent and indexer acknowledgement is not requested, it is
simply deleted from the workqueue. If it could be sent and indexer
acknowledgement is requested, then the ackId returned from the HEC
will be recorded with it and it is moved to eh ackqueue.

Finally the process goes through the messages in the ackqueue, at
most `maxbatchsize` messages per run. For each message, it queries
the HEC whether the message is already acknowledged. If it is
acknowledged, it is removed from the ackqueue. If it is not acknowledged
and the last delivery attempt has been no more than `retry_timer` seconds
ago, it remains in the ackqueue.  If it is not acknowledged and the last
delivery attempt was more than `retry_timer` seconds ago, it is sent to
the HEC again, the new ackId is recorded and it remains in the ackqueue.

